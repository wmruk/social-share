function popUp(href, winWidth, winHeight, resizable) {
    winWidth = winWidth || '550';
    winHeight = winHeight || '450';
    resizable = resizable ? 'yes' : 'no';

    var strTitle = ((typeof document.title !== 'undefined') ? document.title : 'Social Share');
    var strParam = 'width=' + winWidth + ',height=' + winHeight + ',resizable=' + resizable;

    window.open(href, strTitle, strParam).focus();
}

$(document).ready(function ($) {
    $('.customer.share').on("click", function (e) {
        e.preventDefault();
        popUp($(this).attr('href'));
    });
});
